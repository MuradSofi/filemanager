Vue.component('custom-table', {
    mounted() {
      this.goFolder({back: 0});
    },
    data: function() {
        return {
            items: []
        };
    },
    methods: {
      goFolder({back, folderName, method, index})  {
          if(method == "move") {
              var newPath = prompt("Please enter new path", "/");
          }
          var vm = this;
          fetch(`/fileManager/requestHandler.php?back=${back}&folderName=${folderName}&action=${method}&newPath=${newPath}`)
              .then(function(response) {
                  response.json().then(function(parsedJson) {
                      if(back != 7) {
                          vm.items = parsedJson;
                      }else {
                          if(parsedJson.success) {
                              vm.items.splice(index, 1);
                          }else {
                              alert("Xeta bas verdi")
                          }
                      }

                  })
              })
      },
    },
    template: `<table class="table table-striped table-dark">
                <thead>
                    <tr>
                        <td class="w-25">Name</td>
                        <td class="w-25">Size</td>
                        <td class="w-25">Perms</td>
                        <td class="w-25">#</td>
                    </tr>
                </thead>
                <tbody>
                <tr @click="goFolder({back: -1, folderName: null})">
                            <td>../</td>
                            <td></td>
                            <td></td>
                            <td></td>
                </tr>
                <template v-for="(item, key) in items">
                
                     <tr @click="goFolder({back: 1, folderName: item.name, index: key})">
                            <td>{{item.name}}</td>
                            <td>{{item.size}}</td>
                            <td>{{item.perm}}</td>
                            <td>
                                <button type="button" class="btn btn-danger" @click.stop="goFolder({back: 7, folderName: item.name})">Delete</button>
                                <button type="button" class="btn btn-light" @click.stop="goFolder({back: 7, folderName: item.name, method: 'move'})">Move</button>
                            </td>
                     </tr>
                </template>
                   
                </tbody> 
              </table>`
});
