<?php
if(!isset($_COOKIE["baseDir"])) {
    setcookie("baseDir", getcwd(), time() + (86400 * 30), "/");
};

function returnFolderItems($folderPath = false) {

    $folderPath = $folderPath ?: (isset($_COOKIE['baseDir']) ? $_COOKIE['baseDir'] : "/");
    $items = scandir($folderPath);
    $itemsWithSize = [];

    for ($i = 2; $i <= count($items) - 1; $i++) {
        $io = popen ( '/usr/bin/du -sk ' . $items[$i], 'r' );
        $size = filesize($folderPath."/".$items[$i]);
        $perm = fileperms($folderPath."/".$items[$i]);
        $perm = substr(sprintf('%o', $perm), -3);
        $itemsWithSize[$i - 2] = [ "name" => $items[$i], "size" => size2Byte($size), "perm" => $perm ];
    }
    return $itemsWithSize;
}

function size2Byte($size) {
    $units = array('KB', 'MB', 'GB', 'TB');
    $currUnit = '';
    while (count($units) > 0  &&  $size > 1024) {
        $currUnit = array_shift($units);
        $size /= 1024;
    }
    return ($size | 0) . $currUnit;
}
