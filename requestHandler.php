<?php
require_once("returnFolderInsideItems.php");
$back = $_GET["back"];
$folderName = isset($_GET['folderName']) ? $_GET['folderName'] : "";
$action = isset($_GET['action']) ? $_GET['action'] : "delete";
$newPath = isset($_GET['newPath']) ? $_GET['newPath'] : "/";
switch ($back) {
    case -1:
        $baseDir = isset($_COOKIE['baseDir']) ? $_COOKIE['baseDir'] : "/";
        $path = explode("/", $baseDir);
        array_pop($path);
        $path = implode("/",$path);
        setcookie("baseDir", $path, time() + (86400 * 30), "/");
        header("HTTP/1.1 200 OK");
        echo  json_encode(returnFolderItems($path));
        break;
    case 0:
        header("HTTP/1.1 200 OK");
        echo  json_encode(returnFolderItems( $_COOKIE['baseDir']));
        break;
    case 1:
        $baseDir = isset($_COOKIE['baseDir']) ? $_COOKIE['baseDir'] : "/";
        $path = $baseDir."/".$folderName;
        setcookie("baseDir", $path, time() + (86400 * 30), "/");
        echo  json_encode(returnFolderItems($path));
    case 7:
        if($action == "delete") {
            $baseDir = isset($_COOKIE['baseDir']) ? $_COOKIE['baseDir'] : "/";
            $path = $baseDir."/".$folderName;
            if(rmrf($path)) {
                header("HTTP/1.1 200 OK");
                echo json_encode([
                    "success" => false,
                ]);
            }else {
                header("HTTP/1.1 200 OK");
                echo json_encode([
                    "success" => true,
                ]);
            }
        }else {
            $baseDir = isset($_COOKIE['baseDir']) ? $_COOKIE['baseDir'] : "/";
            $path = $baseDir."/".$folderName;
            if(!@rename($path, $newPath."/".basename($path))) {
                header("HTTP/1.1 200 OK");
                echo json_encode([
                    "success" => false,
                ]);
            }else {
                header("HTTP/1.1 200 OK");
                echo json_encode([
                    "success" => true,
                ]);
            }

        }
}

function rmrf($dir) {
    $flag = false;
    foreach (glob($dir) as $file) {
        if (is_dir($file)) {
            rmrf("$file/*");
            if( @rmdir($file) !== true ) {
                $flag = true;
            }else {
                $flag = false;
            }
        } else {
            if ( file_exists($file) ) {
                if( @unlink($file) !== true ) {
                    $flag = true;
                }else {
                    $flag = false;
                }
            }else {
                $flag = false;
            }
        }
    }
    return $flag;
}
